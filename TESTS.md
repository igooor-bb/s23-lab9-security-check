# Testing

sut: <https://openweathermap.org>

## Test Case 1: Forgot Password

<https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-service>

| Test step  | Result |
|---|---|
| Open openweathermap.org  | ✅ |
| Press "Sign In" button  | ✅ "Sign In" page is opened |
| Press "Click here to recover" button | ✅ Textfield for email appeared |
| Enter non-existing email and submit | ✅ Message "Email not found" appeared (Image 1) |
| Enter existing email | ✅ Message about sending recovery instructions appeared. (Image 2) Email was delivered. |

Image 1
![Email Not Found](.images/email_not_found.png)

Image 2
![Email Found](.images/email_found.png)

Results:

- OpenWeatherMap properly shows messages about password recovery according OWASP cheat sheet.

- Sometimes OpenWeatherMap shows CAPTCHA when trying to recover password many times in a row.

## Test Case 2: REST Security (HTTPS)

<https://cheatsheetseries.owasp.org/cheatsheets/REST_Security_Cheat_Sheet.html#https>

> Secure REST services must only provide HTTPS endpoints.

| Test step  | Result |
|---|---|
| Open openweathermap.org  | ✅ |
| Open REST API documentation | ✅ |
| Find API calls and ensure the addresses contain HTTPS protocol | ✅ Endpoints are secured with HTTPS (Image 3) |

Results:

- OpenWeatherMap REST API endpoints are secured with HTTPS 👍🏻

## Test Case 3: REST Security (API Keys)

<https://cheatsheetseries.owasp.org/cheatsheets/REST_Security_Cheat_Sheet.html#api-keys>

> Public REST services without access control run the risk of being farmed leading to excessive bills for bandwidth or compute cycles. API keys can be used to mitigate this risk.

| Test step  | Result |
|---|---|
| Open openweathermap.org  | ✅ |
| Open REST API documentation | ✅ |
| Find API calls and ensure they contain API key as a parameter | ✅ API calls contain API Key (Image 3) |
| Log In to the website | ✅ Profiled button appeared |
| Press on the "Profile" button | ✅ Context menu appeared |
| Press on the "My API Keys" button | ✅ API keys page opened |
| Try to generate a key | ✅ Key is generated (Image 4) |
| Send a request and ensure it works | ✅ Request works (Image 5) |

Image 3
![HTTPS](.images/https.png)

Image 4
![API Keys](.images/keys.png)

Image 5
![API Call](.images/api_call.png)

Results:

- OpenWeatherMap has API keys for API requests

- They can be easily generated using corresponding tab in profile screen

- API calls work with API key and does not without it.

## Test Case 4: Email Validation

<https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html#email-address-validation>

| Test step  | Result |
|---|---|
| Open openweathermap.org  | ✅ |
| Push "Create an account" button | ✅ Account creation screen opened  |
| Enter email without "@" | ✅ Warning appeared (Image 6) |
| Enter email `ig~~~r'';bb@yandex.ru` | ❗️ Account registered successfully (Image 7), while email has invalid characters and cannot be registered in yandex mail |

Image 6
![Invalid Email](.images/email_is_invalid.png)

Image 7
![Confirmation Sent](.images/confirmation_sent.png)

Result:

- OpenWeatherMap has basic validation of email address

- The validation is weak since it does not consider dangerous and invalid characters.
